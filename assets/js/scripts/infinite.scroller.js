;(function(context) {
	
	var 
		$window,
		$document,
		debounce,
		methods;
	
	if(context) {
		debounce = context.debounce;
	} else {
		debounce = require('./debounce');
	}
	
	d = debounce();
	$document = $(document);
	$window = $(window);
	
	var infiniteScrollers = [];

	var InfiniteScroll = function($el) {
		var self = this;
		this.$el = $el;
		this.reqPending
		this.$el.data('infiniteScroller',this);
		this.req = null;
		this.url = this.$el.data('url');
		this.params = this.$el.data('params') || {};
		infiniteScrollers.push(this);

		this.$el.trigger('infiniteScrollInit',[self]);
	};

	InfiniteScroll.prototype.doRequest = function() {
		var self = this;
		
		//if the ajax request is initialized and not finished, skip this
		if(this.req && this.req.readyState !== 4) { return; }

		this.req = $.ajax({
			url:this.url,
			data:this.params
		})

		this.req
			.then(
				function(r,status,jqXHR) {
					self.$el.trigger('infiniteScrollSuccess',[r,self]);
				},
				function(r,status,jqXHR) {
					status != 'aborted' && self.$el.trigger('infiniteScrollFailed',[r,self]);
				}
			);

		this.$el.trigger('infiniteScrollStartedReq',[self]);
	};

	InfiniteScroll.prototype.setURL = function(url) {
		this.url = url;
	};

	InfiniteScroll.prototype.setParameter = function(k,v) {
		this.params[k] = v;
		return this;
	};

	InfiniteScroll.prototype.setParameters = function(obj) {
		var self = this;
		var keys = obj.keys();
		keys.forEach(function(k) {
			self.params[k] = obj[k];
		})
	}

	InfiniteScroll.prototype.hasReachedBottom = function() {
		//double check, make sure this.$el exists in the DOM
		return jQuery.contains(document.documentElement, this.$el[0]) && window.scrollY + window.innerHeight >= this.$el.offset().top + this.$el[0].clientHeight;
	};

	InfiniteScroll.prototype.destroy = function() {
		infiniteScrollers.splice(infiniteScrollers.indexOf(this),1);

		//remove the instance of the infinite scroller from
		//the data attached to this element so it can be GCed
		this.$el.removeData('infiniteScroller');
	};
	
	$document
		.on('updateTemplate.infiniteScroll ready',function(e) {
			$('.infinite-scroller')
				.filter(function() {
					return !$(this).data('infiniteScroller')
				})
				.each(function() {
					(new InfiniteScroll($(this)));
				})
		});
		
	var lastScrollPosition;
	$window
		.on('resize scroll',function() {
			if(!lastScrollPosition) {
				lastScrollPosition = window.scrollY;
			}

			//this was a scroll up - nothing to do here
			if(lastScrollPosition > window.scrollY) { return; }

			d.requestProcess(function() {

				if(window.scrollY + window.innerHeight)

				infiniteScrollers.forEach(function(is) {
					is.hasReachedBottom() && is.doRequest();
				});
			});
		});

	//How to Use
	/*
	$('#my-infinite-scroll-element')
		.on('infiniteScrollInit',function(e,is) {
			//We can set parameters in here if we want (you can also do this on the element directly in the HTML)
			is
				.setParameter('param1','value1')
				.setParameter('param2','value2')
		})
		.on('infiniteScrollStartedReq',function(e,is) {
			//You could use this event to add a class,
			//or start to display a loading notification
		})
		.on('infiniteScrollSuccess',function(e,r,is) {

			//append the response, probably by doing this, but it depends
			//on what you are returning from the server ("r")
			$(this).append(r.html);

			//update the parameters to send to the server the next time
			//we hit bottom and need to load more results
			is.setParameter('page',r.nextPage);

			//if we're not getting any more results because we've loaded
			//everything and you're confident we never need to do anything
			//on scroll again with this element, you can call destroy
			is.destroy();

		})
		.on('infiniteScrollFailed',function(e,r,is) {
			//uh oh, something went wrong
		});
	*/

}(typeof ns !== 'undefined' ? window[ns] : undefined));