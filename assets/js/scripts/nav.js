;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		methods,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$body = $('body'),
		$navWrap  =$('.nav-wrap'),
		$nav = $('nav'),
		$html = $('html'),
		$searchForm = $('.global-search-form'),
		SHOW_NAV_CLASS = 'show-nav',
		COLLAPSE_NAV_AT = 1280,
		SHOW_SEARCH_CLASS = 'show-search',
		SHOW_FIXED_NAV_CLASS = 'fixed-nav';

	methods = {
	
		showNav: function(show) {
			if(show) {
				$html.addClass(SHOW_NAV_CLASS);
			} else {
				$html.removeClass(SHOW_NAV_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_NAV_CLASS);
		},

		checkShowSmallNav: function() {
			if(window.innerWidth > COLLAPSE_NAV_AT) {
				$body.addClass(SHOW_FIXED_NAV_CLASS);
			} else {
				$body.removeClass(SHOW_FIXED_NAV_CLASS);
			}
		},
		
		isNavCollapsed: function() {
			return window.innerWidth < COLLAPSE_NAV_AT;
		},
		
		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
		},

		showSearch: function(show) {
			var $input = $('input',$searchForm);
			
			if(show) {
				$html.addClass(SHOW_SEARCH_CLASS);
				setTimeout(function() { $input.focus(); },500);
			} else {
				$html.removeClass(SHOW_SEARCH_CLASS);
				$input.blur();
			}
		},
		
		toggleSearch: function() {
			this.showSearch(!this.isShowingSearch());
		},
		
		isShowingSearch: function() {
			return $html.hasClass(SHOW_SEARCH_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.showNav(false);
		})		
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				
				if(methods.isShowingSearch()) {
					methods.showSearch(false);
					return false;
				}
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		})
		.on('click','.toggle-search',function() {
			methods.toggleSearch();
		})
		.on('click','nav span.drop',function() {

			var $this = $(this);

			$this
				.closest('li')
				.siblings()
				.find('ul')
				.stop()
				.slideUp();


			$this
				.siblings()
				.slideToggle();

		});

	$window
		.on('scroll',function() { methods.onScroll(); })
		.on('resize',function() { methods.onResize(); });
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));