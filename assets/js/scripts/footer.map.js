;(function(context) {

	var 
		scrollTo,
		methods,
		$footerMapWrap = $('.footer-map-wrap'),
		EXPANDED_CLASS = 'expanded';

	if(context) {
		scrollTo = context.scrollTo;
	} else {
		scrollTo = require('./plugins/scroll.to.js');
	}

	methods = {

		toggle: function() {
			if(this.isExpanded()) {
				this.hide();
			} else {
				this.show();
			}
		},

		show: function() {
			$footerMapWrap.addClass(EXPANDED_CLASS);
		},

		hide: function() {
			$footerMapWrap.removeClass(EXPANDED_CLASS);
		},

		scrollTo: function() {
			this.show();
			scrollTo($footerMapWrap);
		},

		isExpanded: function() {
			return $footerMapWrap.hasClass(EXPANDED_CLASS);
		}

	};

	$(document)
		.on('click','.scroll-to-map',function() {
			methods.scrollTo();
		})
		.on('click','.toggle-map',function() {
			methods.toggle();
		})
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));