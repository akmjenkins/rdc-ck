;(function(context) {


	if(context) {
		// @codekit-prepend "../../../bower_components/circles/circles.js"
	} else {
		require('../../../bower_components/circles/circles.js');
	}

	var 
		$swiperWrapper,
		$overlayEl,
		$swiperEl,
		$swipeItems,
		$swiperNavItems,
		MANUAL_SCROLL = false,
		SWIPER_NAV_CLASS_SELECTED = 'selected';


	var methods = {

		init: function() {

			//already inited
			if($swiperWrapper) { return; }

			$swiperWrapper = $('.profile-swiper-wrapper');
			$overlayEl = $('.profile-overlay');
			$swiperEl = $('.profile-swiper');
			$swipeItems = $('.swipe-item',$swiperEl);
			$swiperNavItems = $('.profile-nav-item',$overlayEl);

			$swiperEl.find('.slick-next,.slick-prev').addClass('disabled');

			//add listeners
			var $choosers = $('.profile-chooser-list');
			$choosers
				.each(function() {
					var 
						$this = $(this),
						$labels = $this.find('label'),
						index = $choosers.index(this);

					$this
						.on('scroll',function() {
							$this.data('scrolled',true);
							if(MANUAL_SCROLL) { return; }
							methods.onScroll($this,$labels);
						})
						.on('mouseleave',function() {
							if(methods.getSwiper().currentSlide === index) {
								methods.getSelectedLabel($this).length && methods.onScrollEnd($this,$labels);
							}
						});

				})
				.on('mouseenter',function() {
					//allow this area to be scrolled
					$(this).attr('tabindex',-1).focus().blur();
				})

			$('.profile-progress').each(function() {
				var id,$el = $(this);
				if(!this.id) {
					this.id = methods.generateRandomId();
				}

				id = this.id;

				$(this).data('circle',Circles.create({
					id:id,
					maxValue:100,
					width:32,
					radius:$el.width()/2,
					duration:400,
					colors:['#222350','#8A2976']
				}));		
			});
		},

		goTo: function(i) {

			//verify that slides are finished
			this.getSwiper().goTo(i);
		},

		getSwiper: function() {
			return $swiperWrapper.data('slick');
		},

		generateRandomId: function() {
			return "a"+Math.ceil(Math.random(1,100)*100)*Date.now();
		},

		onSwiperAfterChange: function(index) {
			var $currentElement = $swipeItems.eq(index);
			var $progressCircle = $currentElement.find('.profile-progress');

			if($progressCircle.length) {
				$progressCircle.data('circle').update($progressCircle.data('progress'));
			}

			$swiperWrapper.find('.slick-prev,.slick-next').removeClass('disabled');

			if(index === 0) {
				$swiperWrapper.find('.slick-prev').addClass('disabled');
			} else if(index === this.getSwiper().slideCount-1) {
				$swiperWrapper.find('.slick-next').addClass('disabled');
			}

			if(!this.getSelectedLabelForSlideAtIndex(index).length) {
				$swiperWrapper.find('.slick-next').addClass('disabled');
			} else {
				$swiperWrapper.find('.slick-next').removeClass('disabled');
			}
		},

		onSwiperBeforeChange: function(index) {
			$swiperNavItems
				.removeClass(SWIPER_NAV_CLASS_SELECTED)
				.eq(index)
				.addClass(SWIPER_NAV_CLASS_SELECTED);
		},

		onResize: function() {
			if(!$overlayEl || !$overlayEl.length) { return ; }
			$overlayEl.find('.profile-progress').each(function() {
				var $this = $(this);
				$this.data('circle').updateRadius($this.width()/2);
			})	
		},

		setSelectedLabel: function($label,moveTo,advanceSlide) {
			var 
				parentElement,
				self = this,
				currentSlide = this.getSwiper().currentSlide;
			$label.find('input').prop('checked','checked');

			$swiperNavItems
				.eq(currentSlide).attr('data-percent','100%')
				.end()
				.eq(currentSlide+1)
				.removeClass('unavailable');

			if(moveTo) {
  				parentElement = $label[0].parentElement;
				var pos = (-1 * (parentElement.clientHeight/2) + $label[0].offsetTop) + ($label[0].offsetHeight/2);
				if(parentElement.scrollTop === pos) { return; }

				$swiperWrapper
					.find('.slick-next')
					.removeClass('disabled')
					.addClass('animated shake')
					.on('animationEnd MSAnimationEnd oanimationend webkitAnimationEnd',function() {
						$(this).removeClass('animated shake');
					});

  				MANUAL_SCROLL = true;

  				//crazy ass magic math
  				$(parentElement).animate({
  					scrollTop: pos
  				}, 200, function() {
  					MANUAL_SCROLL = false;
  					advanceSlide && self.advanceSlide();
  				});

			}
		},

		advanceSlide: function() {
			//this.getSwiper().next();
		},

		getSelectedLabelForSlideAtIndex: function(i) {
			return this.getSelectedLabel($('.profile-chooser-list').eq(i));
		},

		getSelectedLabel: function($el) {
			return $el ? $el.find('input:checked') : null;
		},

		getCenteredLabel: function($el,$labels) {
			var
				$selectedLabel,
				el = $el[0],
				height = el.clientHeight,
				center = height/2;

			$labels.each(function() {
				var $this = $(this);
				$selectedLabel = ($this.position().top < center) ? $this : $selectedLabel;
			});
			return $selectedLabel;
		},

		onScrollEnd: function($el,$labels) {
			this.setSelectedLabel(this.getCenteredLabel($el,$labels),true);
		},

		onScroll: function($el,$labels) {
			this.setSelectedLabel(
				this.getCenteredLabel($el,$labels),
				false
			);
		}
	};

	//global listeners
	$(document)
		.on('profileOverlayLoaded',function() {
			methods.init();
		})
		.on('beforeChange','.profile-overlay',function(e,slick,curr,next) {
			methods.onSwiperBeforeChange(next);
		})	
		.on('afterChange','.profile-overlay',function(e,slick,next) {
			methods.onSwiperAfterChange(next);
		})			
		.on('click','.profile-nav-item',function() {
			var $el = $(this);

			if($el.hasClass('unavailable')) {
				$('.profile-chooser-list')
					.eq(methods.getSwiper().currentSlide)
					.addClass('shake animated')
					.on('animationEnd webkitAnimationEnd MSAnimationEnd oanimationend',function() {
						$(this).removeClass('shake');
					});
				return;
			}
			methods.goTo($swiperNavItems.index(this));
		})
		.on('click','.profile-chooser-list label',function(){
			methods.setSelectedLabel($(this),true,true);
		});

	$(window)
		.on('resize',function() { 
			methods.onResize(); 
		});

}(typeof ns !== 'undefined' ? window[ns] : undefined));