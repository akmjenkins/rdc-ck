;(function(context) {

	var tests;
	var debounce;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();

	$(document)
		.on('updateTemplate.lazyYT',function() {
			
			$('div.lazyyt')
				.filter(function() {
					return !$(this).hasClass('.lazyYT-container');
				})
				.each(function() { 
					$(this).lazyYT(); 
				});	
				
		})
		.trigger('updateTemplate.lazyYT')
		
		//grid-action-share
		.on('click','.grid-item-actions .action-share, .grid-item-share .close-share',function() { 
			$(this).closest('.item').toggleClass('show-share'); 
		})

		//card filter
		.on('mousedown','.card-filter-other-toggle',function() {
			$(this).closest('.card-items-filter-bar').toggleClass('show-other');
		})

		//explore button
		.on('click','.explore-button',function() {
			$('body').toggleClass('show-explore')
		})

		//impact overlay
		.on('click','.toggle-impact-overlay',function(e) {
			e.preventDefault();

			var 
				req = true,
				$body = $('body'),
				src = $(this).data('src'),
				isOnPage = $('.impact-overlay').length;

			if(!isOnPage) {
				req = $.ajax({
					url:src
				}).then(function(r,status,jqXHR) {
					$body.append(r);
				});
			}

			$.when(req)
				.then(function() {
					$body.toggleClass('show-impact');

					if($body.hasClass('show-impact')) {
						//give it time to build the charts
						$(document).trigger('updateTemplate');
						$('.hero .swiper-wrapper').data('slick').pause();
					} else {
						$('.hero .swiper-wrapper').data('slick').play();
					}
					
				});

		})
		.on('click','.impact-overlay .toggle-filter-bar',function() {
			$('.impact-filter').toggleClass('show-options');
		})

		//profile overlay
		.on('click','.toggle-profile-overlay',function(e) {
			e.preventDefault();

			var 
				req = true,
				$body = $('body'),
				src = $(this).data('src'),
				isOnPage = $('.profile-overlay').length;

			if(!isOnPage) {
				req = $.ajax({
					url:src
				}).then(function(r,status,jqXHR) {
					$body.append(r);
				});
			}

			$.when(req)
				.then(function() {
					$body.toggleClass('show-profile');

					if($body.hasClass('show-profile')) {
						$(document)
							.trigger('updateTemplate')
							.trigger('profileOverlayLoaded');
						$('.hero .swiper-wrapper').data('slick').pause();
					} else {
						$('.hero .swiper-wrapper').data('slick').play();
					}
				});	
		})

	$(document)
		.on(
			{
				infiniteScrollInit: function(e,is) {
				},

				infiniteScrollStartedReq: function(e,is) {
					//show a loader in here
					$(this).addClass('is-loading');
				},

				infiniteScrollSuccess: function(e,r,is) {
					var $el = $(this);
					$el.removeClass('is-loading');

					//update the "page" parameter so the next time 
					//the infinite scroll makes a request it will
					//pass in the correct page value
					is.setParameter('page',r.next_page);

					if(r.html) {
						$el.append(r.html);
					} else {
						//alert('Nothing else!');
						is.destroy();
					}
				},

				infiniteScrollFailed: function(e,r,is) {
					$(this).removeClass('is-loading');
				}
			},
			'#projects-scroller'
		);

	
}(typeof ns !== 'undefined' ? window[ns] : undefined));