;(function(context) {

	//load all required scripts	
	require('./scripts/anchors.external.popup.js');
	require('./scripts/standard.accordion.js');
	require('./scripts/custom.select.js');
	require('./scripts/magnific.popup.js');
	require('./scripts/lazy.images.js');
	require('./scripts/nav.js');
	require('./scripts/lazyyt.js');
	require('./scripts/chartist.wrapper.js');
	require('./scripts/swiper.js');
	require('./scripts/profile.user.js');
	
	//global
	require('./global.js');
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));