<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<div>
			<h1 class="hgroup-title">Rise Above the Ice</h1>
			<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
		</div>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">The Latest</a>
				<a href="#">News Item Title</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section class="pad20">
		<div class="sw">
			<h2>Latest</h2>
		</div><!-- .sw -->
			
		<div class="grid card-grid nopad">
			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/juan-casanova.jpg"></div>

					<span class="card-tag card-ico fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Juan Casanova</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/lesley-james.jpg"></div>

					<span class="card-tag card-ico fa-ship">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Lesley James</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/ed-clark.jpg"></div>

					<span class="card-tag card-ico fa-bolt">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Ed Clarke</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->

		<br>

		<div class="center">
			<a href="#" class="primary button fill uc">View All</a>
		</div><!-- .center -->

		<hr class="light">
	</section>

	<section class="pad20">
		<div class="sw">
			<h2>Latest Projects</h2>
		</div><!-- .sw -->
			
		<div class="grid card-grid nopad">
			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/juan-casanova.jpg"></div>

					<span class="card-tag card-ico fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Juan Casanova</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/lesley-james.jpg"></div>

					<span class="card-tag card-ico fa-ship">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Lesley James</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/ed-clark.jpg"></div>

					<span class="card-tag card-ico fa-bolt">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Ed Clarke</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->

		<br>

		<div class="center">
			<a href="#" class="primary button fill uc">View All</a>
		</div><!-- .center -->

		<hr class="light">
	</section>

	<section class="pad20">
		<div class="sw">
			<h2>Latest News &amp; Events</h2>
		</div><!-- .sw -->
			
		<div class="grid card-grid nopad">
			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/juan-casanova.jpg"></div>

					<span class="card-tag">News</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Juan Casanova</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/lesley-james.jpg"></div>

					<span class="card-tag">Event</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Lesley James</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/ed-clark.jpg"></div>

					<span class="card-tag">News</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Ed Clarke</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->

		<br>

		<div class="center">
			<a href="#" class="primary button fill uc">View All</a>
		</div><!-- .center -->

		<hr class="light">
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>