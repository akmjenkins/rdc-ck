<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<?php include('inc/i-page-actions.php'); ?>

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Our Future</h1>
			<span class="hgroup-subtitle">Providing Leadership, Strategic Focus and Financial Investments</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">

			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Connect</a>
				<a href="#">Who We Are</a>
				<a href="#">Our Goal</a>
			</div><!-- .crumb-links -->

			<div class="site-links">
				<a href="#">Stories</a>	
				<a href="#" class="selected">Who We Are</a>
				<a href="#">Industries</a>
				<a href="#">Facilities</a>
			</div><!-- .site-links -->

		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					
					<div class="article-body">
						
						<strong>Over the next ten years, RDC will focus on the following activities:</strong>

						<ul class="two-col">
							<li>
								Increasing overall R&D investment in Newfoundland and Labrador
							</li>
							<li>
								Pursuing R&D opportunities that are relevant to the local economy
							</li>
							<li>
								Targeting sectors that are of strategic importance to Newfoundland and Labrador’s economy 
							</li>
							<li>
								Understanding current and future markets and research needs
							</li>
							<li>
								Responding quickly and flexibly to opportunities
							</li>
							<li>
								Encouraging key stakeholders to collaborate and cooperate in the R&D process 
							</li>
						</ul><!-- .two-col -->

						<strong>RDC investment decisions are guided by the need to:</strong>

						<ul class="two-col">
							<li>
								Increase business investment in R&D
							</li>
							<li>
								Target industries with identified technical needs, market opportunities or resource development potential
							</li>
							<li>
								Invest in the development of highly-qualified researchers, innovative research equipment, and world-class research facilities
							</li>
							<li>
								Emphasize quality, industry relevance and economic impact when considering institutional investments in R&D
							</li>
							<li>
								Employ a global lens in evaluating opportunities to ensure investment decisions are strategic and aligned with Newfoundland and Labrador’s competitive advantages 
							</li>
							<li>
								Encouraging key stakeholders to collaborate and cooperate in the R&D process 
							</li>
						</ul>

					</div><!-- .article-body -->

				</div><!-- .content -->
				<div class="sidebar sidebar-primary always-first">

					<div class="sidebar-mod section-links-mod">
						<h4>In This Section</h4>
						<ul>
							<li><a href="#">Our Goal</a></li>
							<li><a class="selected" href="#">Our Future</a></li>
							<li><a href="#">Governance</a></li>
							<li><a href="#">Suppliers</a></li>
						</ul>	
					</div><!-- .sidebar-mod -->

				</div><!-- .sidebar-primary -->

			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>