<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<?php include('inc/i-page-actions.php'); ?>

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Facilities</h1>
			<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-tag">Featured Project</span>

						<span class="hero-content-title">Donec Elementum Nunc sed Nibh.</span>		
						<span class="hero-content-subtitle">Mauris dictum ligula lectus non accumsan</span>
						<a href="#link" class="hero-content-link">Explore &raquo;</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discover</a>
				<a href="#">Facilities</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p class="excerpt">
							Canatec Associates International had a crushing problem on its hands. The company had successfully developed harsh environment 
							communications technology for the detection, analysis, forecasting and management of sea ice.
						</p><!-- .excerpt -->

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->
			
		</div><!-- .sw -->
	</section>

	<section class="nopad">

		<div class="facility-block d-bg lazybg" data-src="../assets/images/temp/facilities/facility-1.jpg">
			
			<div class="facility-content">
				<h2>Facility One</h2>

				<p>
					Nunc tempus maximus purus, non hendrerit lacus laoreet a. Nunc vel vulputate tortor, molestie bibendum sapien. 
					In hendrerit erat id dolor suscipit semper. Sed sed leo consectetur, sagittis velit vel, lacinia lorem. Nulla facilisi. 
					Nullam consectetur lectus ac leo aliquam, sed fringilla elit rhoncus. Quisque dictum posuere tempus. Duis rhoncus 
					porttitor neque id lacinia. Curabitur arcu velit, efficitur ut nunc eget, elementum scelerisque ante.
				</p>

				<a href="#" class="button fill primary grad">More Info</a>
			</div><!-- .facility-content -->

		</div><!-- .facility-block -->

		<div class="facility-block d-bg lazybg" data-src="../assets/images/temp/facilities/facility-2.jpg">
			
			<div class="facility-content">
				<h2>Facility Two</h2>

				<p>
					Nunc tempus maximus purus, non hendrerit lacus laoreet a. Nunc vel vulputate tortor, molestie bibendum sapien. 
					In hendrerit erat id dolor suscipit semper. Sed sed leo consectetur, sagittis velit vel, lacinia lorem. Nulla facilisi. 
					Nullam consectetur lectus ac leo aliquam, sed fringilla elit rhoncus. Quisque dictum posuere tempus. Duis rhoncus 
					porttitor neque id lacinia. Curabitur arcu velit, efficitur ut nunc eget, elementum scelerisque ante.
				</p>

				<a href="#" class="button fill primary grad">More Info</a>
			</div><!-- .facility-content -->

		</div><!-- .facility-block -->

		<div class="facility-block d-bg lazybg" data-src="../assets/images/temp/facilities/facility-3.jpg">
			
			<div class="facility-content">
				<h2>Facility Three</h2>

				<p>
					Nunc tempus maximus purus, non hendrerit lacus laoreet a. Nunc vel vulputate tortor, molestie bibendum sapien. 
					In hendrerit erat id dolor suscipit semper. Sed sed leo consectetur, sagittis velit vel, lacinia lorem. Nulla facilisi. 
					Nullam consectetur lectus ac leo aliquam, sed fringilla elit rhoncus. Quisque dictum posuere tempus. Duis rhoncus 
					porttitor neque id lacinia. Curabitur arcu velit, efficitur ut nunc eget, elementum scelerisque ante.
				</p>

				<a href="#" class="button fill primary grad">More Info</a>
			</div><!-- .facility-content -->

		</div><!-- .facility-block -->

		<div class="facility-block d-bg lazybg" data-src="../assets/images/temp/facilities/facility-4.jpg">
			
			<div class="facility-content">
				<h2>Facility Four</h2>

				<p>
					Nunc tempus maximus purus, non hendrerit lacus laoreet a. Nunc vel vulputate tortor, molestie bibendum sapien. 
					In hendrerit erat id dolor suscipit semper. Sed sed leo consectetur, sagittis velit vel, lacinia lorem. Nulla facilisi. 
					Nullam consectetur lectus ac leo aliquam, sed fringilla elit rhoncus. Quisque dictum posuere tempus. Duis rhoncus 
					porttitor neque id lacinia. Curabitur arcu velit, efficitur ut nunc eget, elementum scelerisque ante.
				</p>

				<a href="#" class="button fill primary grad">More Info</a>
			</div><!-- .facility-content -->

		</div><!-- .facility-block -->
	</section>



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>