<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<?php include('inc/i-page-actions.php'); ?>

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Who We Are</h1>
			<span class="hgroup-subtitle">Providing Leadership, Strategic Focus and Financial Investments</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">

			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Connect</a>
				<a href="#">Who We Are</a>
			</div><!-- .crumb-links -->

			<div class="site-links">
				<a href="#">Stories</a>	
				<a href="#" class="selected">Who We Are</a>
				<a href="#">Industries</a>
				<a href="#">Facilities</a>
			</div><!-- .site-links -->

		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					
					<div class="article-body">
						
						<h3>RDC is a provincial Crown corporation established to improve research and development (R&D) in Newfoundland and Labrador.</h3>

						<p>
							We work with research and development stakeholders including business, academia and government agencies and departments 
							to make strategic investments in highly qualified people, R&D infrastructure and innovative research. 
						</p>

						<p>
							In December, 2008, The Research and Development Council Act was passed by the Government of Newfoundland and Labrador 
							establishing the Research & Development Corporation as an arms-length provincial Crown corporation with provision 
							for an independent Board of Directors. Since its launch, RDC has received an annual budget from the provincial legislature
							 dedicated to R&amp;D funding programs.
						</p>

					</div><!-- .article-body -->

				</div><!-- .content -->
				<div class="sidebar sidebar-primary always-first">

					<div class="sidebar-mod section-links-mod">
						<h4>In This Section</h4>
						<ul>
							<li><a href="#">Our Goal</a></li>
							<li><a href="#">Our Future</a></li>
							<li><a href="#">Governance</a></li>
							<li><a href="#">Suppliers</a></li>
						</ul>	
					</div><!-- .sidebar-mod -->

				</div><!-- .sidebar-primary -->

			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>