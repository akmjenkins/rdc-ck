<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-time">
	<div class="sw">
		<time datetime="2014-10-20" class="styled">
			<span class="m">Oct</span>
			<span class="d">20</span>
			<span class="y">2014</span>
		</time>
		<div>
			<h1 class="hgroup-title">Rise Above the Ice</h1>
			<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
		</div>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discover</a>
				<a href="#">Stories</a>
				<a href="#">Rise Above the Ice</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p class="excerpt">
							Canatec Associates International had a crushing problem on its hands. The company had successfully developed harsh environment 
							communications technology for the detection, analysis, forecasting and management of sea ice.
						</p><!-- .excerpt -->

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->

			<div class="full-article-bg lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="2:1"></div>

			<div class="main-body">
				<div class="content">

					<div class="article-body">

						<p class="excerpt">
							The technology could transmit effectively at minus 40 degrees and demonstrated battery life better 
							than anyone else in the business.
						</p>

						<p>
							The trouble was the beacon housings were designed primarily to land and sit on arctic ice floes, not to survive crushing 
							between floes, conditions that occur as the ice melts and refreezes in spring and fall.
						</p>
							
						<p> 
							Seeing the promise of its technology, RDC invested in a Canatec R&D project to build an ice beacon that would work better 
							in all seasonal ice environments and in a wider variety of ice zones. The drift buoys were designed, developed and tested 
							in simulated laboratory ice. Now C-CORE’s Centre for Arctic Resource Development (CARD) is deploying them as part of a 
							project to improve understanding of the dynamics and seasonal break-up of near-shore ice.
						</p>
							
						<p> 
							And Canatec is discovering new markets for the emerging technology. With more help from RDC, the ice beacons are now 
							morphing into a completely new product to solve problems involving marine search and rescue, with worldwide markets in 
							view, not just the Arctic. It’s an R&D success story that rises to the occasion, no matter what the ocean conditions.
						</p>

					</div><!-- .article-body -->

				</div><!-- .content -->
				
				<aside class="sidebar">
					<div class="sidebar-mod chart-mod d-bg secondary-bg item">
						
						<h3>Investment Breakdown</h3>

						<div class="chart-wrap with-labels">
							<div 
								class="chart chartist-pie"
								data-chart-type="pie"
								data-show-tooltips="true"
								data-chart-data='<?php 
									echo json_encode(
										array(
											'series'=>
												array(
													33,
													8,
													19,
													40
												),
											'labels'=> 
												array(
													'Label One - 33%',
													'Label Two - 8%',
													'Label Three - 19%',
													'Label Four - 40%'
												)
										)
									);
								?>'>
							</div>
						</div>

						<?php include('inc/i-grid-item-actions.php'); ?>
						<?php include('inc/i-grid-item-share.php'); ?>
					</div>
				</aside><!-- .sidebar -->

			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="nopad">
		<div class="article-footer-gallery">

			<div class="swiper-wrapper">
				<div class="swiper"
					data-arrows="true"
					data-slides-to-show="2"
					data-slides-to-scroll="2"
					data-responsive='<?php
						echo json_encode(
									array(
										array(
											'breakpoint' => 750,
											'settings' => array(
												'slidesToShow' => 1,
												'slidesToScroll' => 1
											)
										)
									)
							);
					?>'
					data-update-lazy-images="true">

					<!--  you may put as many images here as you want -->
					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/stories-single-1.jpg"></div>
					</div><!-- .swipe-item -->

					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/stories-single-2.jpg"></div>
					</div><!-- .swipe-item -->

					<!--  you may put as many images here as you want -->
					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/stories-single-1.jpg"></div>
					</div><!-- .swipe-item -->

					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/stories-single-2.jpg"></div>
					</div><!-- .swipe-item -->

				</div><!-- .swiper -->
			</div><!-- .swiper-wrapper -->

		</div><!-- .article-footer-gallery -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>