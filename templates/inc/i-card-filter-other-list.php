			<div class="card-filter-other-list light-bg">
				<ul>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Advanced Manufacturing</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Aerospace &amp; Defence</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Aquaculture</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Fisheries</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Forestry &amp; Agriculture</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Health &amp; Life Sciences</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">ICT (Information &amp; Communications Tech)</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Marine Science</span>
							<span class="label-count">50</span>
						</label>
					</li>
					<li>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Other</span>
							<span class="label-count">50</span>
						</label>
					</li>
				</ul>
			</div><!-- .card-filter-other-list -->