<div class="grid-item-share">

	<button class="t-fa-abs fa-close close-share">Close</button>

	<div>
		<span>Share</span>
		<div class="share-icons social">
			<a href="#" rel="external" class="social-fb">Share This on Facebook</a>
			<a href="#" rel="external" class="social-tw">Share This on Twitter</a>
			<a href="#" rel="external" class="social-in">Share This on LinkedIn</a>
			<a href="#" rel="external" class="social-gp">Share This on Google Plus</a>
		</div><!-- .share-icons -->
	</div>
</div><!-- .grid-item-share -->