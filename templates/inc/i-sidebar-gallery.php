<div class="sidebar-gallery">

	<div class="grid pad20 fill">

		<div class="col col-2">

			<!-- 
				href is path to full size image
			-->
			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-1.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-1.jpg"></div>
			</a>

		</div><!-- .col -->

		<div class="col col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-2.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-2.jpg"></div>
			</a>

		</div><!-- .col -->	

		<div class="col col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-3.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-3.jpg"></div>
			</a>

		</div><!-- .col -->

		<div class="col col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-4.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-4.jpg"></div>
			</a>

		</div><!-- .col -->	

		<div class="col lg-col-3 md-col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-5.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-5.jpg"></div>
			</a>

		</div><!-- .col -->		

		<div class="col lg-col-3 md-col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-6.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-6.jpg"></div>
			</a>

		</div><!-- .col -->	

		<div class="col lg-col-3 md-col-2">

			<a class="bounce item mpopup" href="../assets/images/temp/sidebar-gallery/gal-7.jpg" data-gallery="sidebar-gallery">
				<div class="img sidebar-gallery-item lazybg" data-src="../assets/images/temp/sidebar-gallery/gal-7.jpg"></div>
			</a>

		</div><!-- .col -->	

	</div><!-- .grid -->

</div><!-- .sidebar-gallery -->