		<div class="impact-overlay d-bg secondary-bg">
			<div class="sw">
				
				<button class="t-fa fa-times-circle toggle-impact-overlay toggle-overlay-btn">Close</button>

				<div class="impact-header">
					
					<div class="impact-title">
						Making an
						<span>Impact</span>
					</div><!-- .impact-title -->

					<div class="impact-info">
						RDC is committed to strengthening R&D collaboration among businesses, academia and government. 
						Here is a glimpse into what we've done.
					</div><!-- .impact-info -->

				</div><!-- .impact-header -->

				<div class="impact-filter card-items-filter-bar">

					<button class="toggle-filter-bar">Options</button>

					<div class="card-filter-bar-items impact-filter-bar-items">
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Minerals/Mining</span>
							<span class="label-count">50</span>
						</label>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Ocean Tech</span>
							<span class="label-count">50</span>
						</label>
						<label class="card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Energy</span>
							<span class="label-count">50</span>
						</label>
						<label class="with-more card-filter-other-toggle card-filter-item">
							<input type="checkbox" name="project_type[]" value="1">
							<span class="label-name">Other</span>
						</label>
					</div><!-- .card-filter-buttons -->	

					<div class="card-filter-bar-items">
						<label class="card-filter-item">
							<input type="checkbox" name="story_type[]" value="1">
							<span class="label-name">All</span>
							<span class="label-count">50</span>
						</label>
						<label class="card-filter-item">
							<input type="checkbox" name="story_type[]" value="2">
							<span class="label-name">Stories</span>
							<span class="label-count">50</span>
						</label>
						<label class="card-filter-item">
							<input type="checkbox" name="story_type[]" value="3">
							<span class="label-name">Stats</span>
							<span class="label-count">50</span>
						</label>
						<button class="t-fa fa-file-text-o pdf-button" id="impact-pdf">Compile as PDF</button>	
					</div><!-- .card-filter-bar-items -->

					<?php include('i-card-filter-other-list.php'); ?>
										
				</div><!-- .impact-filter -->				

				<div class="impact-content">
					<div class="grid nopad eqh card-grid">

						<div class="col">
							<div class="item card-item bounce">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce with-expanded-content">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/sherri-christian.jpg"></div>

								<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Sherri Christian</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<div class="card-item-expanded-content">
										<div class="lazyyt immediate" data-youtube-id="5LJZm7bpSdA" data-ratio="5:3"></div>
									</div><!-- .card-item-expanded-content -->

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->		

						<div class="col">
							<div class="item card-item card-item-stats style-tertiary">
								<div class="card-item-content">
									<div class="card-item-title">
										<span class="big">$46.7+</span> Million
									</div>

									<p>
										Total R&amp;D Spending including RDC investment and leveraged R&amp;D commitment.
									</p>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item card-item-pie-chart">

								<span class="chart-title">
									Investment Breakdown
									<small>for 2013-14</small>
								</span><!-- .chart-title -->

								<div class="chart-wrap with-labels">
									<div 
										class="chart chartist-pie"
										data-chart-type="pie"
										data-show-tooltips="true"
										data-chart-data='<?php 
											echo json_encode(
												array(
													'series'=>
														array(
															33,
															8,
															19,
															40
														),
													'labels'=> 
														array(
															'Label One - 33%',
															'Label Two - 8%',
															'Label Three - 19%',
															'Label Four - 40%'
														)
												)
											);
										?>'>
									</div>
								</div>

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce with-expanded-content">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<div class="card-item-expanded-content">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet 
											lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque.
										</p>
										<a href="#" class="expanded-link">Read More &raquo;</a>
									</div><!-- .expanded-content -->

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col card-col-2">
							<div class="item card-item content-card light-bg">

								<div class="split-card">
									<div class="split-card-content">
										<h4 class="card-title">Year by year breakdown of contributions by RDC to the economy.</h4>

										<p>
											Elementum ante quis commodo vulputate. Nam tempus a erat eu dictum. Aenean et tristique tortor. 
											Morbi at facilisis tortor. Donec nibh massa, luctus nec facilisis quis, pretium tempus elit. 
											Fusce accumsan aliquam libero quis vehicula. Quisque non enim metus.
										</p>
									</div><!-- .split-card-content -->
									<div class="split-card-content">
										<div class="chart-wrap bar-chart-wrap">
											<div 
												class="chart ct-chart chartist-bar simple-bar-chart"
												data-chart-type="bar"

												data-chart-options='<?php
													echo json_encode(
														array(
															'axisY' => array(
																'showLabel' => false
															)
														)
													)
												?>'

												data-chart-data='<?php 
													echo json_encode(
														array(
															'series'=>
																array(
																	array(
																		100,
																		85,
																		90,
																		65,
																		70,
																		90,
																		70
																	)
																),
															'labels'=> 
																array(
																	'‘08',
																	'‘09',
																	'‘10',
																	'‘11',
																	'‘12',
																	'‘13',
																	'‘14'
																)
														)
													);
												?>'>
											</div>
										</div>
									</div><!-- .split-card-content -->
								</div><!-- .split-card -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item bounce with-expanded-content">
								<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

								<span class="card-ico card-tag fa-diamond">&nbsp;</span>

								<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

								<div class="card-item-content">
									<span class="card-item-title">Steve Piercey</span>
									<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

									<div class="card-item-expanded-content">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet 
											lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque.
										</p>
										<a href="#" class="expanded-link">Read More &raquo;</a>
									</div><!-- .expanded-content -->

									<a href="#" class="card-item-link">Explore &raquo;</a>
								</div><!-- .card-item-content -->

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

						<div class="col">
							<div class="item card-item card-item-pie-chart">

								<span class="chart-title">
									Investment Breakdown
									<small>for 2013-14</small>
								</span><!-- .chart-title -->

								<div class="chart-wrap with-labels">
									<div 
										class="chart chartist-pie"
										data-chart-type="pie"
										data-show-tooltips="true"
										data-chart-data='<?php 
											echo json_encode(
												array(
													'series'=>
														array(
															33,
															8,
															19,
															40
														),
													'labels'=> 
														array(
															'Label One',
															'Label Two',
															'Label Three',
															'Label Four'
														)
												)
											);
										?>'>
									</div>
								</div>

								<?php include('inc/i-grid-item-actions.php'); ?>
								<?php include('inc/i-grid-item-share.php'); ?>
							</div><!-- .card-item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				</div><!-- .impact-content -->

			</div><!-- .sw -->
		</div><!-- .impact-overlay -->