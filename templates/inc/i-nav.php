<div class="mobile-nav-bg"></div>

<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>


<div class="outer-nav-wrap">
	<div class="nav-wrap">
	
		<div class="inner-nav-wrap">
			<nav>

				<a href="#" class="nav-logo lazybg with-img">
					<img src="../assets/images/rdc-logo.svg" alt="Research &amp; Development Corporation Newfoundland and Labrador">
				</a>

				<ul>
					<li>
						<span class="drop">Discover</span>
						<ul>
							<li><a href="#">Stories</a></li>
							<li><a href="#">Who We Are</a></li>
							<li><a href="#">Industries</a></li>
							<li><a href="#">Facilities</a></li>
						</ul>
					</li>
					<li>
						<span class="drop">Connect</span>
						<ul>
							<li><a href="#">Projects</a></li>
							<li><a href="#">Academic</a></li>
							<li><a href="#">Business</a></li>
						</ul>
					</li>
					<li>
						<span class="drop">Funding</span>
						<ul>
							<li><a href="#">Academic Research</a></li>
							<li><a href="#">Business Funding</a></li>
							<li><a href="#">Claims &amp; Awards</a></li>
						</ul>
					</li>
				</ul>

				<div class="meta-nav">
					<a href="#">Media</a>
					<a href="#">The Latest</a>
					<a href="#">Contact</a>
				</div><!-- .meta-nav -->

				<button class="button block fill primary grad toggle-impact-overlay" data-src="inc/i-impact-overlay.php">Impact</button>

				<?php include('i-social.php'); ?>
			</nav>
		</div><!-- .inner-nav-wrap -->

	</div><!-- .nav-wrap -->
</div><!-- .outer-nav-wrap -->