<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Research and Development Corporation NL</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../bower_components/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,initial-scale=1.0">
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/images/favicons/favicon.ico?">
		<link rel="icon" type="image/png"  href="../assets/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/images/favicons/favicon-144.png">

		<!-- slickslider -->
		<link rel="stylesheet" href="../bower_components/slick.js/slick/slick.css">
		<script src="../bower_components/slick.js/slick/slick.min.js"></script>

		<!-- magnific popup -->
		<link rel="stylesheet" href="../bower_components/magnific-popup/dist/magnific-popup.css">
		<script src="../bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>		

		<!-- chartist -->
		<link rel="stylesheet" href="../bower_components/chartist/dist/chartist.min.css">
		<script src="../bower_components/chartist/dist/chartist.min.js"></script>	

		<!-- theme -->
		<link rel="stylesheet" href="../assets/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
		
		<header>
			<div class="sw full">

				<a href="#" class="header-logo lazybg with-img">
					<img src="../assets/images/rdc-logo-light.svg" alt="Research &amp; Development Corporation Newfoundland and Labrador">
				</a>

				<div class="header-meta">
					<a href="#" class="user-link" data-src="inc/i-profile-overlay.php">
						<span style="background-image: url(../assets/images/temp/user-ico.jpg);"></span> Username
					</a>
					<a href="#">Media</a>
					<a href="#">The Latest</a>
					<a href="#">Contact</a>
					<button class="t-fa-abs fa-search toggle-search">Search</button>
				</div><!-- .header-meta -->

			</div><!-- .sw -->
		</header>
	
		<div class="page-wrapper">	