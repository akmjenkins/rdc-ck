<?php

	//test
	usleep(500000);

$card = <<<CARD
	<div class="col">
		<a href="#" class="item card-item bounce">

			<span class="card-ico card-tag fa-diamond">&nbsp;</span>

			<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

			<div class="card-item-content">
				<span class="card-item-title">SULIS 1i: 3D Subsea Inspection Tool</span>

				<span class="card-item-link">Explore &raquo;</span>
			</div><!-- .card-item-content -->
		</a><!-- .card-item -->
	</div><!-- .col -->
CARD;

	$response = array(
		'page' => $_GET['page'],
		'next_page' => $_GET['page']+1
	);

	if($_GET['page'] <= 3) {
		$response['html'] = implode('',array($card,$card,$card));
	}

	header('Content-Type: application/json');
	echo json_encode($response);