<div class="social">
	<a href="#" title="Like RDC on Facebook" class="social-fb" rel="external">Like RDC on Facebook</a>
	<a href="#" title="Follow RDC on Twitter" class="social-tw" rel="external">Follow RDC on Twitter</a>
	<a href="#" title="Connect with RDC on LinkedIn" class="social-in" rel="external">Connect with RDC on LinkedIn</a>
	<a href="#" title="Add RDC to your circles on Google Plus" class="social-gp" rel="external">Add RDC to your circles on Google Plus</a>
</div><!-- .social -->