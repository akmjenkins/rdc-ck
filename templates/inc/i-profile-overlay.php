		<div class="profile-overlay d-bg secondary-bg">

			<div class="sw">
				<button class="t-fa fa-times-circle toggle-profile-overlay toggle-overlay-btn">Close</button>
			</div><!-- .sw -->

			<div class="swiper-wrapper profile-swiper-wrapper">
			
				<div class="profile-swiper swiper"
					data-infinite="false"
					data-draggable="false"
					data-swipe="false">

					<div class="swipe-item">

						<div class="profile-swipe-item-content">

							<div class="profile-progress-wrap">
								<div class="profile-progress" data-progress="0"></div>
							</div><!-- .profile-progress-wrap -->

							<div class="profile-chooser">

								<div class="profile-chooser-label">
									<span>I am a</span>
								</div>

								<div class="profile-chooser-list-wrap">
									<div class="profile-chooser-list-inner-wrap">

										<div class="profile-chooser-list">

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_type" value="business">
												<span>
													<span>Business</span>
												</span>
											</label>

											<label class="t-fa fa-bar-chart">
												<input tabindex="-1" type="radio" name="user_type" value="researcher">
												<span>
													<span>Researcher</span>
												</span>
											</label>

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_type" value="student">
												<span>
													<span>Student</span>
												</span>
											</label>

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_type" value="entreprenuer">
												<span>
													<span>Entrepreneur</span>
												</span>
											</label>

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_type" value="business">
												<span>
													<span>Business</span>
												</span>
											</label>
										</div><!-- .profile-chooser-list -->

									</div><!-- .profile-chooser-list-inner-wrap -->

								</div><!-- .profile-chooster-list -->

							</div><!-- .profile-chooser -->

						</div><!-- .profile-swipe-item-content -->

					</div><!-- .swipe-item -->

					<div class="swipe-item">

						<div class="profile-swipe-item-content">

							<div class="profile-progress-wrap">
								<div class="profile-progress" data-progress="50"></div>
							</div><!-- .profile-progress-wrap -->

							<div class="profile-chooser">

								<div class="profile-chooser-label">
									<span>Interested In</span>
								</div>

								<div class="profile-chooser-list-wrap">
									<div class="profile-chooser-list-inner-wrap">

										<div class="profile-chooser-list">

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_interest[]" value="business">
												<span>
													<span>Business</span>
												</span>
											</label>

											<label class="t-fa fa-bolt">
												<input tabindex="-1" type="radio" name="user_interest[]" value="energy">
												<span>
													<span>Energy</span>
												</span>
											</label>

											<label class="t-fa fa-ship">
												<input tabindex="-1" type="radio" name="user_interest[]" value="ocean-tech">
												<span>
													<span>Ocean Tech</span>
												</span>
											</label>

											<label class="t-fa fa-diamond">
												<input tabindex="-1" type="radio" name="user_interest[]" value="minerals-mining">
												<span>
													<span>Minerals &amp; Mining</span>
												</span>
											</label>

											<label class="t-fa fa-user">
												<input tabindex="-1" type="radio" name="user_interest[]" value="business">
												<span>
													<span>Business</span>
												</span>
											</label>
										</div><!-- .profile-chooser-list -->

									</div><!-- .profile-chooser-list-inner-wrap -->
								</div><!-- .profile-chooser-list -->

							</div><!-- .profile-chooser -->

						</div><!-- .profile-swipe-item-content -->
						
					</div><!-- .swipe-item -->

					<div class="swipe-item">
						<div class="profile-swipe-item-content login-item">

							<div class="body-form full">
								<p>
									Fill out the form below to complete sign up for your account.
								</p>

								<input tabindex="-1" type="text" name="name" placeholder="Full Name">
								<input tabindex="-1" type="email" name="email" placeholder="E-mail Address">
								<input tabindex="-1" type="password" name="password" placeholder="Password">
								<input tabindex="-1" type="password" name="confirm_password" placeholder="Confirm Password">

								<button tabindex="-1" class="button fill primary" type="Submit">Submit</button>
							</div><!-- .body-form -->

							<div class="social-login-block">
								<a href="#" tabindex="-1" class="button primary fill social-login social-login-facebook">Sign in using Facebook</a>
								<a href="#" tabindex="-1" class="button primary fill social-login social-login-google">Sign in using Google</a>

								<p>
									By clicking Sign In with Facebook or Google, I acknowledge and agree to the 
									<a tabindex="-1" href="#" class="inline">Terms of Use</a> and <a tabindex="-1" href="#" class="inline">Privacy Policy</a>.
								</p>
							</div><!-- .social-login-block -->

						</div><!-- .profile-swipe-item-content -->
						
					</div><!-- .swipe-item -->

				</div><!-- .swiper -->

				<div class="profile-nav">
					
					<button class="profile-nav-item selected" data-percent="0%">
						Career
					</button>
							<hr>
					<button class="profile-nav-item unavailable" data-percent="0%">
						Sector
					</button>
							<hr>
					<button class="profile-nav-item unavailable" data-percent="0%">
						Login
					</button>

				</div><!-- .profile-nav -->

			</div><!-- .swiper-wrapper -->

		</div><!-- .profile-overlay -->