		<div class="card-filter-bar card-items-filter-bar">
			
			<div class="card-filter-bar-items">
				<label class="card-filter-item">
					<input type="checkbox" name="project_type[]" value="1">
					<span class="label-name t-fa fa-diamond">Minerals &amp; Mining</span>
					<span class="label-count">50</span>
				</label>
				<label class="card-filter-item">
					<input type="checkbox" name="project_type[]" value="1">
					<span class="label-name t-fa fa-ship">Ocean Technology</span>
					<span class="label-count">50</span>
				</label>
				<label class="card-filter-item">
					<input type="checkbox" name="project_type[]" value="1">
					<span class="label-name t-fa fa-bolt">Energy</span>
					<span class="label-count">50</span>
				</label>
				<label class="with-more card-filter-other-toggle card-filter-item">
					<input type="checkbox" name="project_type[]" value="1">
					<span class="label-name t-fa fa-ellipsis-h">Other</span>
				</label>
			</div><!-- .card-filter-buttons -->

			<?php include('i-card-filter-other-list.php'); ?>
		</div><!-- .card-filter-bar -->