<form action="/" method="get" class="global-search-form">
	<div class="fieldset">
		<input type="search" name="s" placeholder="Type your search and press enter...">
		<span class="close t-fa fa-times-circle-o toggle-search">Cancel</span>
	</div>
</form><!-- .global-search-form -->	