<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<h1 class="hgroup-title">Stories</h1>
		<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-tag">Featured Story</span>

						<span class="hero-content-title">Donec Elementum Nunc sed Nibh.</span>		
						<span class="hero-content-subtitle">Mauris dictum ligula lectus non accumsan</span>
						<a href="#link" class="hero-content-link">Explore &raquo;</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discover</a>
				<a href="#">Stories</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section class="filter-section nopad">
		<div class="filter-bar">
			<div class="sw filter-bar-content">
				
				<div class="filter-bar-left">
					Viewing 6 of 20 Stories
				</div>

				<div class="filter-bar-meta">
					
					<form action="/" class="single-form search-form">
						<div class="fieldset">
							<input type="text" name="s" placeholder="Search Stories...">
							<button class="t-fa-abs fa-search">Search</button>
						</div><!-- .fieldset -->
					</form><!-- .single-form -->

					<div class="filter-controls">
						<button class="previous">Previous</button>
						<button class="next">Next</button>
					</div>

				</div><!-- .filter-bar-meta -->


			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		<div class="filter-content">
			
			<div class="grid nopad eqh card-grid">

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

						<span class="card-ico card-tag fa-diamond">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

						<div class="card-item-content">
							<span class="card-item-title">Steve Piercey</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/sherri-christian.jpg"></div>

						<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

						<div class="card-item-content">
							<span class="card-item-title">Sherri Christian</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->		

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/instrumar.jpg"></div>

						<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

						<div class="card-item-content">
							<span class="card-item-title">Instrumar</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/juan-casanova.jpg"></div>

						<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
						<div class="card-item-content">
							<span class="card-item-title">Juan Casanova</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->	

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/lesley-james.jpg"></div>

						<span class="card-ico card-tag fa-ship">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
						<div class="card-item-content">
							<span class="card-item-title">Lesley James</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->			

				<div class="col">
					<a href="#" class="item card-item bounce">
						<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/ed-clark.jpg"></div>

						<span class="card-ico card-tag fa-bolt">&nbsp;</span>

						<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

						<div class="card-item-content">
							<span class="card-item-title">Ed Clarke</span>
							<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

							<span class="card-item-link">Explore &raquo;</span>
						</div><!-- .card-item-content -->
					</a><!-- .card-item -->
				</div><!-- .col -->	
			</div><!-- .grid -->

		</div><!-- .filter-content -->
	</section>



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>