<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<div>
			<h1 class="hgroup-title">Search Results</h1>
			<span class="hgroup-subtitle">Showing <span class="primary">10</span> results for <span class="primary">"Query"</span>.</span>
		</div>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="body">

	<section class="nopad">
		<div class="grid-head">
			<div class="sw grid-head-content">
				<h2 class="grid-head-title">2 Pages Found</h2>
				<div class="filter-controls">
					<button class="previous">Previous</button>
					<button class="next">Next</button>
				</div><!-- .filter-controls -->	
			</div><!-- .grid-head-content -->		
		</div><!-- .grid-head -->
			
		<div class="grid card-grid nopad">

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/instrumar.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">Page Title</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/instrumar.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">Page Title</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

		</div><!-- .grid -->
	</section>

	<section class="nopad">
		<div class="grid-head">
			<div class="sw grid-head-content">
				<h2 class="grid-head-title">3 Stories Found</h2>
				<div class="filter-controls">
					<button class="previous">Previous</button>
					<button class="next">Next</button>
				</div><!-- .filter-controls -->	
			</div><!-- .grid-head-content -->		
		</div><!-- .grid-head -->
			
		<div class="grid card-grid nopad">
			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->		

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>

	<section class="nopad">
		<div class="grid-head">
			<div class="sw grid-head-content">
				<h2 class="grid-head-title">3 Projects Found</h2>
				<div class="filter-controls">
					<button class="previous">Previous</button>
					<button class="next">Next</button>
				</div><!-- .filter-controls -->	
			</div><!-- .grid-head-content -->		
		</div><!-- .grid-head -->
			
		<div class="grid card-grid nopad">
			<div class="col">
				<a href="#" class="item card-item bounce">

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->		

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>

	<section class="nopad">
		<div class="grid-head">
			<div class="sw grid-head-content">
				<h2 class="grid-head-title">2 Claims Found</h2>
				<div class="filter-controls">
					<button class="previous">Previous</button>
					<button class="next">Next</button>
				</div><!-- .filter-controls -->	
			</div><!-- .grid-head-content -->		
		</div><!-- .grid-head -->
			
		<div class="grid infoblock-grid nopad">
			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Leverage R&amp;D</span>
						<p>
							attracts public funding for academic-led research and development (R&D) in areas relevant to both industry and the Newfoundland and Labrador economy.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Ignite R&amp;D</span>
						<p>
							attracts highly-qualified academic researchers and builds new research and development (R&D) capacity in areas relevant to both industry and the Newfoundland and Labrador economy.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>