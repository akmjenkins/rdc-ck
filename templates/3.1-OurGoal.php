<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<div class="page-meta-actions">
		<!-- add a class of "is-favourite" if the article is a favourite -->

		<button class="favourite toggle-favourite">Favourite</button>
		<button class="share">Share</button>
	</div><!-- .page-meta-actions -->

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Our Goal</h1>
			<span class="hgroup-subtitle">Providing Leadership, Strategic Focus and Financial Investments</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">

			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Connect</a>
				<a href="#">Who We Are</a>
				<a href="#">Our Goal</a>
			</div><!-- .crumb-links -->

			<div class="site-links">
				<a href="#">Stories</a>	
				<a href="#" class="selected">Who We Are</a>
				<a href="#">Industries</a>
				<a href="#">Facilities</a>
			</div><!-- .site-links -->

		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					
					<div class="article-body">
						
						<p>
							Our goal is to provide leadership, strategic focus and financial investments to strengthen the focus, quantity, quality and relevance of R&D in the province and elsewhere for the long-term economic benefit of the province. Increased R&D activities will play a major role in driving innovation, creating wealth and increasing economic growth in Newfoundland and Labrador for future generations
						</p>

					</div><!-- .article-body -->

				</div><!-- .content -->
				<div class="sidebar sidebar-primary always-first">

					<div class="sidebar-mod section-links-mod">
						<h4>In This Section</h4>
						<ul>
							<li><a class="selected" href="#">Our Goal</a></li>
							<li><a href="#">Our Future</a></li>
							<li><a href="#">Governance</a></li>
							<li><a href="#">Suppliers</a></li>
						</ul>	
					</div><!-- .sidebar-mod -->

				</div><!-- .sidebar-primary -->

			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>