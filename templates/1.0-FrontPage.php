<?php $bodyclass = 'home '; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-title">Invest. Innovate. Impact.</span>		
						<a href="#" data-src="inc/i-profile-overlay.php" class="toggle-profile-overlay button primary fill grad t-fa fa-user">Build Your Profile</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home-i.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-title">Impact. Innovate. Invest.</span>
						<a href="#" data-src="inc/i-profile-overlay.php" class="toggle-profile-overlay button primary fill grad t-fa fa-user">Build Your Profile</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

	<div class="explore-button-wrap">
		<button class="explore-button">Explore</button>
	</div><!-- .explore-button-wrap -->

</div><!-- .hero -->

<div class="body">


	<section class="nopad explore-section">

		<?php include('inc/i-card-filter-bar.php'); ?>

		<div class="grid nopad eqh card-grid">

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/steve-piercey.jpg"></div>

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Steve Piercey</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/sherri-christian.jpg"></div>

					<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Sherri Christian</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->		

			<div class="col">
				<a href="#" class="item card-item card-item-stats">
					<div class="card-item-content">
						<div class="card-item-title">
							<span class="big">500+</span> Projects
						</div>
						<span class="card-item-link">See our impact &raquo;</span>
					</div>
				</a><!-- .card-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/juan-casanova.jpg"></div>

					<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Juan Casanova</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/lesley-james.jpg"></div>

					<span class="card-ico card-tag fa-ship">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Lesley James</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/ed-clark.jpg"></div>

					<span class="card-ico card-tag fa-bolt">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Ed Clarke</span>
						<span class="card-item-info">Nulla iaculis sapien at consequat viverra</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->

	</section><!-- .explore-section -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>