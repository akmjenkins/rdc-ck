<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<h1 class="hgroup-title">Projects</h1>
		<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-tag">Featured Project</span>

						<span class="hero-content-title">Donec Elementum Nunc sed Nibh.</span>		
						<span class="hero-content-subtitle">Mauris dictum ligula lectus non accumsan</span>
						<a href="#link" class="hero-content-link">Explore &raquo;</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discover</a>
				<a href="#">Projects</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section class="nopad">

		<?php include('inc/i-card-filter-bar.php'); ?>	
			
		<!--
			data-url:
				The source URL that will be requested when the infinite scroller makes a request

			data-params:
				The parameters that will be sent along when the infinite scroller makes it's first request

				NOTE: 
					You can (and probably will) modify these in the JavaScript callback so that
					you can send along different parameters each time a new request is made
					e.g. first time: page=1, second: request page=2, etc...

		-->

		<div class="grid nopad eqh card-grid infinite-scroller" 
			id="projects-scroller"
			data-url="./inc/cards-ajax.php"
			data-params='<?php
				echo json_encode(array(
					"param1" => "value1",
					"page" => 2
				))
			?>'>

			<div class="col">
				<a href="#" class="item card-item bounce">

					<span class="card-ico card-tag fa-diamond">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">

						<!-- PLEASE trim() this title and add an ellipsis if over 100 characters -->
						<span class="card-item-title">SULIS 1i: 3D Subsea Inspection Tool</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item card-item bounce">
					<div class="card-bg lazybg img" data-src="../assets/images/temp/blocks/block-project.jpg"></div>
					<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">
							Extending Simulation-based Learning Beyond the Simulation 
							Laboratory Using Social Networking and Gaming Elements
						</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<div class="item card-item card-item-stats">
					<div class="card-item-content">
						<span class="card-item-title">
							<span class="big">500+</span> Projects
						</span>
						<span class="card-item-info">
							This database contains all research and development investments made by RDC since inception in 2009. Project 
							descriptions and additional information are available upon reques
						</span>
					</div>
				</div><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<span class="card-ico card-tag fa-ellipsis-h">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Advancement of a Secure, Zero Error Inline Control, Collating and Packaging Solution</span>
						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item card-item bounce">
					<span class="card-ico card-tag fa-ship">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>
					<div class="card-item-content">
						<span class="card-item-title">Development of Meshless and Discrete Element Methods for Numerical Simulation of Sea Ice</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->			

			<div class="col">
				<a href="#" class="item card-item bounce">
					<span class="card-ico card-tag fa-bolt">&nbsp;</span>

					<time class="card-item-meta" pubdate datetime="2015-04-29">April 29, 2015</time>

					<div class="card-item-content">
						<span class="card-item-title">Real-time Infrastructure Monitoring</span>

						<span class="card-item-link">Explore &raquo;</span>
					</div><!-- .card-item-content -->
				</a><!-- .card-item -->
			</div><!-- .col -->	
		</div><!-- .grid -->

	</section>



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>