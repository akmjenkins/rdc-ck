<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-time">

	<div class="sw">
		<time datetime="2014-10-20" class="styled">
			<span class="m">Oct</span>
			<span class="d">20</span>
			<span class="y">2014</span>
		</time>
		<div>
			<h1 class="hgroup-title">Extending Simulation-based Learning Beyond the Simulation Laboratory Using Social Networking and Gaming Elements</h1>
		</div>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-project.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Extending Simulation-based Learning Beyond the Simulation Laboratory Using Social Networking and Gaming Elements</a>				
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">
			<div class="main-body">
				<div class="content">
					<div class="article-body">
						
						<div class="grid collapse-750">

							<div class="col col-2">
								<div class="item">

									<!-- This could also be a blockquote and have the same styling -->
									<p class="excerpt">
										Suspendisse a suscipit diam. Integer et metus blandit, ultrices mi ultrices, vestibulum tortor. 
										In a tincidunt quam. Etiam eget mauris efficitur, tempor mi sed, volutpat magna.
									</p>

									<a href="#" class="button primary fill grad">View Story</a>
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									<p>
										Suspendisse hendrerit neque gravida, consectetur ante quis, convallis augue. Pellentesque habitant morbi 
										tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam erat volutpat. Maecenas eu 
										suscipit ante. Etiam luctus dui ac sapien interdum, ut condimentum risus iaculis. Donec mollis, lorem 
										vehicula feugiat dapibus, purus sem venenatis lacus, eu scelerisque libero nisi nec metus. Maecenas 
										scelerisque tempus tellus porta vulputate.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->

						</div><!-- .grid -->
					

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->
		</div><!-- .sw -->

		<div class="project-grid grid eqh nopad collapse-950">

			<div class="col col-2">
				<div class="item project-item secondary-bg">
					
					<div class="project-item-content">
						<span class="project-item-title">RDC Investment</span>
						<span class="project-item-stat big">$99,500</span>
					</div><!-- .project-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->

			<div class="col col-2">
				<div class="item project-item secondary-bg">
					
					<div class="project-item-content">
						<span class="project-item-title">Project Investment</span>
						<span class="project-item-stat big">$119,000</span>
					</div><!-- .project-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->

			<div class="col col-2">
				<div class="item project-item primary-bg">
					
					<div class="project-item-content">
						<span class="project-item-title">Proponent</span>
						<span class="project-item-stat">Dubrowski, Adam</span>
					</div><!-- .project-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->			

			<div class="col col-2">
				<div class="item project-item primary-bg darker-bg">
					
					<div class="project-item-content">
						<span class="project-item-title">Program Used</span>
						<span class="project-item-stat">R&amp;D Vouchers</span>
					</div><!-- .project-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->			

		</div><!-- .project-grid -->
	</section>



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>