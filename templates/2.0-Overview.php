<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<h1 class="hgroup-title">Discover</h1>
		<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discover</a>				
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">
			<div class="main-body">
				<div class="content">
					<div class="article-body">
						
						<div class="grid collapse-750">

							<div class="col col-2">
								<div class="item">

									<!-- This could also be a blockquote and have the same styling -->
									<p class="excerpt">
										Suspendisse a suscipit diam. Integer et metus blandit, ultrices mi ultrices, vestibulum tortor. 
										In a tincidunt quam. Etiam eget mauris efficitur, tempor mi sed, volutpat magna.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									<p>
										Suspendisse hendrerit neque gravida, consectetur ante quis, convallis augue. Pellentesque habitant morbi 
										tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam erat volutpat. Maecenas eu 
										suscipit ante. Etiam luctus dui ac sapien interdum, ut condimentum risus iaculis. Donec mollis, lorem 
										vehicula feugiat dapibus, purus sem venenatis lacus, eu scelerisque libero nisi nec metus. Maecenas 
										scelerisque tempus tellus porta vulputate.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->

						</div><!-- .grid -->
					

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->
		</div><!-- .sw -->
	</section>

	<section class="nopad">

		<div class="ov-grid grid nopad">

			<div class="col">
				<a href="#" class="item ov-item bounce">
					<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/blocks/ov-stories.jpg"></div>
					<div class="ov-item-content">
						<span class="ov-item-title">Stories</span>
						<span class="ov-item-subtitle">Nunc tempus maximus purus</span>

						<span class="ov-item-link">Explore &raquo;</span>
					</div><!-- .ov-item-content -->
				</a><!-- .ov-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item ov-item bounce">
					<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/blocks/ov-who-we-are.jpg"></div>
					<div class="ov-item-content">
						<span class="ov-item-title">Who We Are</span>
						<span class="ov-item-subtitle">Nunc tempus maximus purus</span>

						<span class="ov-item-link">Explore &raquo;</span>
					</div><!-- .ov-item-content -->
				</a><!-- .ov-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item ov-item bounce">
					<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/blocks/ov-industries.jpg"></div>
					<div class="ov-item-content">
						<span class="ov-item-title">Industries</span>
						<span class="ov-item-subtitle">Nunc tempus maximus purus</span>

						<span class="ov-item-link">Explore &raquo;</span>
					</div><!-- .ov-item-content -->
				</a><!-- .ov-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item ov-item bounce">
					<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/blocks/ov-facilities.jpg"></div>
					<div class="ov-item-content">
						<span class="ov-item-title">Facilities</span>
						<span class="ov-item-subtitle">Nunc tempus maximus purus</span>

						<span class="ov-item-link">Explore &raquo;</span>
					</div><!-- .ov-item-content -->
				</a><!-- .ov-item -->
			</div><!-- .col -->

		</div><!-- .ov-grid -->

	</section><!-- .nopad -->



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>