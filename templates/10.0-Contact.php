<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">
	<div class="sw">
		<div>
			<h1 class="hgroup-title">Who We Are</h1>
			<span class="hgroup-subtitle">Providing Leadership, Strategic Focus and Financial Investments</span>
		</div>
	</div><!-- .sw -->
</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Media</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="grid collapse-850">

				<div class="col col-2-5">
					<div class="item">
						
						<h5>Location and Mailing Address:</h5>

						<address>
							68 Portugal Cove Rd., <br>
							St. John's, NL, A1B 2L9 <br>
							Canada
						</address>

						<br>

						<span class="block">Telephone: +1.709.758.0913</span>
						<span class="block">Fax: +1.709.758.0972</span>
						<span class="block">Email: <a href="#" class="inline">info@rdc.org</a></span>

						<br>

						<span class="block">General Inquiries: <a href="#" class="inline">info@rdc.org</a></span>
						<span class="block">Program Inquiries: <a href="#" class="inline">programs@rdc.org</a></span>
						<span class="block">Application Submission: <a href="#" class="inline">application@rdc.org</a></span>
						<span class="block">Employment Inquiries: <a href="#" class="inline">careers@rdc.org</a></span>

						<br>

						<h5>Additional contacts:</h5>

						<span class="block">RDC Media Contact Information</span>
						<span class="block">RDC CEO Contact Information</span>
						<span class="block">RDC ATIPP Officer Contact Information</span>

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3-5">
					<div class="item">
						<p>Fill out the form below to contact us.</p>
						<form action="/" class="body-form full">
							<div class="fieldset">
								<input type="text" name="name" placeholder="Full Name">
								<input type="email" name="email" placeholder="E-mail Address">
								<input type="text" name="subject" placeholder="Subject">
								<textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>

								<button type="submit" class="button primary fill">Submit</button>
							</div><!-- .fieldset -->
						</form><!-- .body-form -->
					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .sw -->	
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>