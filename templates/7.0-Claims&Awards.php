<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup">

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Claims &amp; Awards</h1>
			<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<span class="hero-content-tag">Featured Project</span>

						<span class="hero-content-title">Donec Elementum Nunc sed Nibh.</span>		
						<span class="hero-content-subtitle">Mauris dictum ligula lectus non accumsan</span>
						<a href="#link" class="hero-content-link">Explore &raquo;</a>

					</div><!-- .hero-content -->
				</div><!-- .hero-content-wrap -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Our Progress</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section class="pad20">
		<div class="sw">
			<h2>Business</h2>
		</div><!-- .sw -->
			
		<div class="grid infoblock-grid nopad eqh">

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">R&amp;D Vouchers</span>
						<p>
							Facilitate business access to local, national and international scientific/technical equipment, expertise and research facilities.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">R&amp;D Proof Of Concept</span>
						<p>
							increases technical capacity and reduces the financial risk of performing research and development (R&D) activities through to proof of concept.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Petroleum R&amp;D Accelerator</span>
						<p>
							strengthens Newfoundland and Labrador’s capacity as a research and development (R&D) performer in support of petroleum exploration, development and operations.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">GeoEXPLORE (2011-13)</span>
						<p>
							a three-year directed research program intended to enhance geoscience research and development (R&D) capacity, collaboration and industry innovation in support of 
							mineral and petroleum exploration and development in Newfoundland and Labrador.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">ArcticTECH (2015-19)</span>
						<p>
							a three-year directed research program intended to enhance research and development (R&amp;D) capacity, 
							collaboration and industry innovation in support of Arctic technology development.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Employ R&amp;D</span>
						<p>
							enables businesses to enhance their research and development (R&D) capacity by providing financial support 
							to recent doctoral (PhD) graduates in science and engineering to pursue R&D careers in the private sector.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>

	<section class="pad20">
		<div class="sw">
			<h2>Academic</h2>
		</div><!-- .sw -->
			
		<div class="grid infoblock-grid nopad eqh">

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Leverage R&amp;D</span>
						<p>
							attracts public funding for academic-led research and development (R&D) in areas relevant to both industry and the Newfoundland and Labrador economy.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Ignite R&amp;D</span>
						<p>
							attracts highly-qualified academic researchers and builds new research and development (R&D) capacity in areas relevant to both industry and the Newfoundland and Labrador economy.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Collaborative R&amp;D</span>
						<p>
							increases research and development (R&D) partnerships and collaboration between academia and industry in areas relevant to the Newfoundland and Labrador economy.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">GeoEXPLORE (2011-13)</span>
						<p>
							a three-year directed research program intended to enhance geoscience research and development (R&D) capacity, collaboration and industry innovation in support of 
							mineral and petroleum exploration and development in Newfoundland and Labrador.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">ArcticTECH (2015-19)</span>
						<p>
							a three-year directed research program intended to enhance research and development (R&amp;D) capacity, 
							collaboration and industry innovation in support of Arctic technology development.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">RDC Mitac Accelerate Internships</span>
						<p>
							provide top Newfoundland and Labrador high school students with the opportunity to attend research-related enrichment programs focused on science, technology, engineering and mathematics.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>

	<section class="pad20">
		<div class="sw">
			<h2>Student</h2>
		</div><!-- .sw -->
			
		<div class="grid infoblock-grid nopad eqh">

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">Ocean Industries Student Research Awards</span>
						<p>
							attract highly-qualified students who are interested in pursuing higher education, industry-relevant research and future employment in Newfoundland and Labrador’s ocean industries.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<div class="col">
				<a href="#" class="item infoblock-item">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">RISE Awards</span>
						<p>
							provide top Newfoundland and Labrador high school students with the opportunity to attend research-related enrichment programs focused on science, technology, engineering and mathematics.
						</p>

						<span class="infoblock-item-link">Download &raquo;</span>
					</div><!-- .infoblock-item-content -->
				</a><!-- .infoblock-item -->
			</div><!-- .col -->	

			<a href="#" class="item card-item card-item-stats">
				<div class="card-item-content">
					<div class="card-item-title">
						<span class="big">500+</span> Projects
					</div>
					<span class="card-item-link">See our impact &raquo;</span>
				</div>
			</a><!-- .card-item -->

			<div class="col">
				<div class="item infoblock-item infoblock-stat">

					<div class="infoblock-item-content">
						<span class="infoblock-item-title">
							<span class="big">$46.7+</span> million
						</span>
						<p>
							Total R&D Spending including RDC investment and leveraged R&D commitment.
						</p>
					</div><!-- .infoblock-item-content -->

					<?php include('inc/i-grid-item-actions.php'); ?>

					<?php include('inc/i-grid-item-share.php'); ?>
					
				</div><!-- .infoblock-item -->
			</div><!-- .col -->	

		</div><!-- .grid -->
	</section>



</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>