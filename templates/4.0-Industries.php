<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Claims &amp; Awards</h1>
			<span class="hgroup-subtitle">Sed blandit feugiat diam.</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">
			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Discovery</a>
				<a href="#">Priority Sectors</a>
			</div><!-- .crumb-links -->
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p class="excerpt">
							Canatec Associates International had a crushing problem on its hands. The company had successfully developed harsh environment 
							communications technology for the detection, analysis, forecasting and management of sea ice.
						</p><!-- .excerpt -->

						<!-- no need to show these graphics on small screens -->
						<div class="full-bg-wrap no-599">
							<div class="full-article-bg lazybg" data-src="../assets/images/temp/industries-fullbg-1.jpg"></div>
						</div><!-- .full-bg-wrap -->

						<h2>Energy</h2>

						<div class="grid collapse-950">
							<div class="col col-2">
								<div class="item">
									<p>
										Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum. Vivamus ut diam ut mauris viverra sodales. 
										Pellentesque tempus ac ipsum eu euismod. Aliquam quis efficitur mauris, nec molestie dui. Sed viverra iaculis 
										mauris. Suspendisse a bibendum urna, et laoreet justo.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item projects-list-wrap">
									
									<div class="projects-list">
										<h4 class="projects-list-title">Projects</h4>

										<a href="#" class="inline">Etiam finibus euismod interdum</a>
										<a href="#" class="inline">Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum.</a>
										<a href="#" class="inline">Vivamus ut diam ut mauris viverra</a>
										<a href="#" class="inline">Class aptent taciti sociosqu</a>
									</div><!-- .projects-list -->

									<div class="projects-count-wrap">
										<a href="#" class="projects-count">
											<span class="big">50 Projects</span>
											<span class="projects-count-link">View Projects &raquo;</span>
										</a><!-- .projects-count -->
									</div><!-- .projects-count-wrap -->

								</div><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->

						<!-- no need to show these graphics on small screens -->
						<div class="full-bg-wrap no-599">
							<div class="full-article-bg lazybg" data-src="../assets/images/temp/industries-fullbg-2.jpg"></div>
						</div><!-- .full-bg-wrap -->

						<h2>Ocean Tech</h2>

						<div class="grid collapse-950">
							<div class="col col-2">
								<div class="item">
									<p>
										Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum. Vivamus ut diam ut mauris viverra sodales. 
										Pellentesque tempus ac ipsum eu euismod. Aliquam quis efficitur mauris, nec molestie dui. Sed viverra iaculis 
										mauris. Suspendisse a bibendum urna, et laoreet justo.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item projects-list-wrap">
									
									<div class="projects-list">
										<h4 class="projects-list-title">Projects</h4>

										<a href="#" class="inline">Etiam finibus euismod interdum</a>
										<a href="#" class="inline">Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum.</a>
										<a href="#" class="inline">Vivamus ut diam ut mauris viverra</a>
										<a href="#" class="inline">Class aptent taciti sociosqu</a>
									</div><!-- .projects-list -->

									<div class="projects-count-wrap">
										<a href="#" class="projects-count">
											<span class="big">50 Projects</span>
											<span class="projects-count-link">View Projects &raquo;</span>
										</a><!-- .projects-count -->
									</div><!-- .projects-count-wrap -->

								</div><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->

						<!-- no need to show these graphics on small screens -->
						<div class="full-bg-wrap no-599">
							<div class="full-article-bg lazybg" data-src="../assets/images/temp/industries-fullbg-3.jpg"></div>
						</div><!-- .full-bg-wrap -->

						<h2>Mining</h2>

						<div class="grid collapse-950">
							<div class="col col-2">
								<div class="item">
									<p>
										Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum. Vivamus ut diam ut mauris viverra sodales. 
										Pellentesque tempus ac ipsum eu euismod. Aliquam quis efficitur mauris, nec molestie dui. Sed viverra iaculis 
										mauris. Suspendisse a bibendum urna, et laoreet justo.
									</p>
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item projects-list-wrap">
									
									<div class="projects-list">
										<h4 class="projects-list-title">Projects</h4>

										<a href="#" class="inline">Etiam finibus euismod interdum</a>
										<a href="#" class="inline">Aliquam laoreet eros sed mi posuere, at iaculis diam fermentum.</a>
										<a href="#" class="inline">Vivamus ut diam ut mauris viverra</a>
										<a href="#" class="inline">Class aptent taciti sociosqu</a>
									</div><!-- .projects-list -->

									<div class="projects-count-wrap">
										<a href="#" class="projects-count">
											<span class="big">50 Projects</span>
											<span class="projects-count-link">View Projects &raquo;</span>
										</a><!-- .projects-count -->
									</div><!-- .projects-count-wrap -->

								</div><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->
			
		</div><!-- .sw -->
	</section>	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>