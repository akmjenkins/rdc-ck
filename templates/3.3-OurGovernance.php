<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="page-title hgroup with-meta-actions">

	<?php include('inc/i-page-actions.php'); ?>

	<div class="sw">
		<div>
			<h1 class="hgroup-title">Governance</h1>
			<span class="hgroup-subtitle">Providing Leadership, Strategic Focus and Financial Investments</span>
		</div>
	</div><!-- .sw -->

</div><!-- .page-title -->

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="true"
			data-dots="true" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw crumb-content">

			<div class="crumb-links">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Connect</a>
				<a href="#">Who We Are</a>
				<a href="#">Governance</a>
			</div><!-- .crumb-links -->

			<div class="site-links">
				<a href="#">Stories</a>	
				<a href="#" class="selected">Who We Are</a>
				<a href="#">Industries</a>
				<a href="#">Facilities</a>
			</div><!-- .site-links -->

		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->

	<section>
		<div class="sw">

			<div class="main-body">
				<div class="content">
					
					<div class="article-body">
						
						<p>
							The Research &amp; Development Corporation (RDC) was established through legislation in 2009, 
							with provision for an independent Board of Directors.
						</p>

						<p>
							The Board comprises both resident and non-residents of the province that represent our key stakeholders, 
							including the private sector, researchers, a publicly-funded post-secondary educational institution, and 
							the provincial government department responsible for innovation.
						</p>

					</div><!-- .article-body -->

				</div><!-- .content -->
				<div class="sidebar sidebar-primary always-first">

					<div class="sidebar-mod section-links-mod">
						<h4>In This Section</h4>
						<ul>
							<li><a href="#">Our Goal</a></li>
							<li><a href="#">Our Future</a></li>
							<li><a class="selected" href="#">Governance</a></li>
							<li><a href="#">Suppliers</a></li>
						</ul>	
					</div><!-- .sidebar-mod -->

				</div><!-- .sidebar-primary -->

			</div><!-- .main-body -->
		</div><!-- .sw -->
	</section>

	<section class="light-bg nopad">
		<div class="sw">
			
			<!-- no need to show these graphics on small screens -->
			<div class="full-bg-wrap no-599">
				<div class="full-article-bg lazybg" data-src="../assets/images/temp/who-we-are-fullbg.jpg">
				</div><!-- .full-bg -->
			</div><!-- .full-bg-wrap -->

			<div class="main-body ">
				<div class="content">

					<div class="article-body">
						<h2>Chief Executive Officer</h2>
						<div class="two-col">
							
							<p>
								Glenn Janes is the Chief Executive Officer of the Research & Development Corporation. He is responsible for providing 
								direction, focus and planning in order to strengthen and improve research and development throughout the Province.
							</p>

							<p>
								Mr. Janes is a Rhodes Scholar. He holds a Master's degree in Environmental Change and Management and a Masters degree 
								in Business Administration, both from Oxford University. He completed a Bachelor of Science in chemistry at Yale University. 
								Mr. Janes obtained the Chartered Director (C.Dir.) designation from The Directors College, a joint venture of McMaster 
								University and The Conference Board of Canada, in 2012.
							</p>

							<p>
								Most recently Mr. Janes was with Imperial Innovations Group plc in London, England, the UK’s leading technology 
								commercialization company. At Imperial Innovations, Mr. Janes was responsible for the development and management of the 
								United Kingdom’s first Recycling Waste Minimization Technology Commercialization Centre.
							</p>

							<p>
								Mr. Janes has held a range of progressively senior positions with companies involved in research, development and technology 
								commercialization processes, including Platina Finance, a London- and Paris-based private equity fund manager. In 
								Newfoundland and Labrador, he has worked with Mad Rock, a developer of safety emergency evacuation sea systems, Hi-Point 
								Industries, Consilient Technologies and Genesis Organic Inc.
							</p>

						</div><!-- .two-col -->
					</div><!-- .article-body -->

				</div><!-- .content -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section>
		<div class="sw">
			
			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<h2>Board Of Directors</h2>

						<div class="grid eqh pad40 collapse-800 director-grid">

							<div class="col col-2">
								<div class="item">
									
									<div class="director-head">
										<div class="director-head-img lazybg with-img">
											<img src="../assets/images/temp/board/fraser-edison.jpg" alt="Fraser H. Edison">
										</div><!-- .director-head-img -->

										<div class="director-info">
											<span class="h4-style director-name">Frasher H. Edison</span>
											<span>Chair of Board</span>
										</div><!-- .director-info -->
									</div>

									<p>
										Fraser H. Edison, President and CEO of Rutter Inc., has extensive management experience in the finance, construction, 
										oil and gas, and transportation industries. He previously led ConPro Group Limited in a joint venture to build the 
										Gravity Based Structure for the Hibernia Project and subsequently the Terra Nova Floating Production and Offloading 
										System. Mr. Edison has served as President of the St. John’s Board of Trade and as the Chair of the Board for the 
										St. John’s International Airport Authority.
									</p>

								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									
									<div class="director-head">
										<div class="director-head-img lazybg with-img">
											<img src="../assets/images/temp/board/fred-cahill.jpg" alt="Fred Cahill">
										</div><!-- .director-head-img -->

										<div class="director-info">
											<span class="h4-style director-name">Fred Cahill</span>
											<span>P. Eng</span>
										</div><!-- .director-info -->
									</div>

									<p>
										Mr. Cahill is President of The Cahill Group of Companies. The group is involved in multi-disciplinary construction 
										and module fabrication in Atlantic Canada and has participated in a number of projects in major industries including 
										oil and gas, mining, power generation, water treatment and institutional sectors. Mr. Cahill is an active member of 
										the community and has held such positions as Chairperson of the Newfoundland and Labrador Construction Association, 
										Chairperson of the Construction Labour Relations Association, Chairperson of MUN’s Genesis Centre, and Member of the 
										Atlantic Canada Energy Round Table.
									</p>

								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									
									<div class="director-head">
										<div class="director-head-img lazybg with-img">
											<img src="../assets/images/temp/board/bernard-collins.jpg" alt="Bernard Collins">
										</div><!-- .director-head-img -->

										<div class="director-info">
											<span class="h4-style director-name">Bernard Collins</span>
											<span>B.BA</span>
										</div><!-- .director-info -->
									</div>

									<p>
										Mr. Collins is the current chairman of PF Collins International Trade Solutions, an internationally recognized 
										expert in trade and logistics solutions for East Coast oil and gas operations. He possesses extensive knowledge 
										of the petroleum industry, ranging from freight to offshore drilling. Mr. Collins is also an award winning 
										entrepreneur, having been named Entrepreneur of the Year by Memorial University’s Gardiner Centre in 2012; twice 
										named as one of the top 50 Atlantic CEOs by Atlantic Business Magazine; and was named the 2005 recipient of 
										NOIA’s Outstanding Contribution Award.
									</p>

								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									
									<div class="director-head">
										<div class="director-head-img lazybg with-img">
											<img src="../assets/images/temp/board/bernard-collins.jpg" alt="Alan Brown">
										</div><!-- .director-head-img -->

										<div class="director-info">
											<span class="h4-style director-name">Alan Brown</span>
											<span>M.Sc., B.Sc., P.Eng., Vice-Chair</span>
										</div><!-- .director-info -->
									</div>

									<p>
										Alan Brown retired from the oil and gas business after many years in technical, operational and business leadership roles around the globe. 
										Most recently he has served as Vice-President, East Coast Canada with Suncor Energy Inc. (formerly Petro-Canada). He now lives in Aberdeen, 
										Scotland, where he maintains contact with a number of energy industry players while endeavouring to support RDC and NL efforts to grow 
										globally competitive technical and business capacity.
									</p>

								</div><!-- .item -->
							</div><!-- .col -->

						</div><!-- .grid -->

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>